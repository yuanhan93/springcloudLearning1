# springcloudLearning1

#### 项目介绍
springcloud微服务第一次简单的学习，熟悉基础，部署和架构
微服务是近两年来比较火的概念，它的含义是：使用定义好边界的小的独立组件来做好一件事情。
Spring Cloud的出现为我们解决分布式开发常用到的问题给出了完整的解决方案。Spring Cloud基于Spring Boot,为我们提供了配置管理、服务发现、断路器、代理服务等我们在做分布式开发时常用问题的解决方案。

#### 软件架构
springcloud

根据实际应用springcloud架构草图

![根据实际应用springcloud架构草图](https://gitee.com/uploads/images/2018/0517/004602_c012bd93_963308.png "springcloud架构草图")

#### 项目启动以及效果图
依次启动discovery(服务发现Eureka Server)、config(配置中心)，后面不分顺序，最后启动monitor(断路器监控)

1.访问 http://localhost:8761/

查看Eureka Server，服务发现

![服务发现](https://gitee.com/uploads/images/2018/0523/221641_ffb3dd57_963308.png "TIM图片20180523221421.png")

2.地址栏输入http://localhost

访问UI服务，走网关代理

![person服务](https://gitee.com/uploads/images/2018/0523/223255_beb92bf3_963308.png "TIM图片20180523222027.png")

![some服务](https://gitee.com/uploads/images/2018/0523/223312_e12bc2fa_963308.png "TIM图片20180523222954.png")

如果发生故障，调用故障处理办法，此处是一个fallback：

![some服务故障](https://gitee.com/uploads/images/2018/0523/223333_185645d2_963308.png "TIM图片20180523222120.png")

3.断路器监控

访问http://localhost:8989/hystrix.stream
![monitor](https://gitee.com/uploads/images/2018/0523/223631_d56c9032_963308.png "TIM图片20180523223101.png")

在Dashboard中输入http://localhost/hystrix.stream，点击Monitor Stream，然后就是下面的效果图：
![断路器具体细节图](https://gitee.com/uploads/images/2018/0523/223645_360bfd79_963308.png "TIM截图20180523223159.png")

