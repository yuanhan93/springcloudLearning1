package com.just.springcloud.streamschemaout1;

import com.just.springcloud.streamschemaout1.model.PersonSource;
import com.just.springcloud.streamschemaout1.schema.MessageStream;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@EnableBinding({MessageStream.class})
public class StreamSchemaOut1ApplicationTests {

    @Autowired
    MessageStream messageStream;
    @Test
    public void contextLoads() {
    }
    /*@Test
    public void testOutput() {
        long start=System.currentTimeMillis();
        for(int i=1;i<=1;i++){
            PersonSource personSource=PersonSource.newBuilder()
                    .setName("小明")
                    .setAge(i)
                    .build();
            this.messageStream.output().send(MessageBuilder.withPayload(personSource).build());
        }
        System.out.println(System.currentTimeMillis()-start);
    }*/

}
