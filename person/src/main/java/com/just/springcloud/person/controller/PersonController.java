package com.just.springcloud.person.controller;

import com.just.springcloud.person.dao.PersonRepository;
import com.just.springcloud.person.domain.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PersonController {
    @Autowired
    private PersonRepository personRepository;
    @RequestMapping(value = "/save",method = RequestMethod.POST)
    public List<Person> savePerson(@RequestBody String personName){
      Person p=new Person(personName);
        personRepository.save(p);
        List<Person> personList=personRepository.findAll(new PageRequest(0,10)).getContent();
        return personList;
    }
}
