package com.just.springcloud.streamclient.service;

import com.just.springcloud.streamclient.model.Person;
import com.just.springcloud.streamclient.model.PersonSource;
import com.just.springcloud.streamclient.service.repository.PersonRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
@Service
//@Transactional(transactionManager = "outputTransactionManager")
public class PersonServiceImpl implements PersonService {
    @Autowired
    private PersonRepository personRepository;
    @Override
    public void insert(PersonSource personSource) {
        Person person=new Person();
        person.setName(personSource.getName().toString());
        person.setAge(personSource.getAge());
        person.setCreateTime(new Date());
        personRepository.save(person);
    }
}
