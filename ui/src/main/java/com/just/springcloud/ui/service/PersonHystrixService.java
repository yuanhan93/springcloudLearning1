package com.just.springcloud.ui.service;

import com.google.common.collect.Lists;
import com.just.springcloud.ui.domain.Person;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 调用person service的断路器
 */
@Service
public class PersonHystrixService {
    @Autowired
    private PersonService personService;
    @HystrixCommand(fallbackMethod = "fallbackSave")
    public List<Person> save(String name){
        return personService.save(name);
    }

    public List<Person> fallbackSave(String name){
        List<Person> personList= Lists.newArrayList();
        Person person=new Person(name+"没有保存成功，person service故障");
        personList.add(person);
        return personList;
    }

}
