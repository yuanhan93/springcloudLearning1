//package com.just.springcloud.streamclient;
//
//import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.boot.context.properties.EnableConfigurationProperties;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Primary;
//import org.springframework.jdbc.datasource.DataSourceTransactionManager;
//
//import javax.sql.DataSource;
//
//@Configuration
//@EnableConfigurationProperties(
//        {       DataSourceConfiguration.InputDataSourceProperties.class,
//                DataSourceConfiguration.OutputDataSourceProperties.class
//        }
//)
//public class DataSourceConfiguration {
//    @ConfigurationProperties(prefix = "spring.datasource.input")
//    public class InputDataSourceProperties extends DataSourceProperties {
//
//    }
//    @ConfigurationProperties(prefix = "spring.datasource.output")
//    public class OutputDataSourceProperties extends DataSourceProperties {
//
//    }
//    @Primary
//    @Bean
//    public DataSource inputDataSource(InputDataSourceProperties properties){
//        return properties.initializeDataSourceBuilder().build();
//    }
//    @Bean
//    public DataSource outputDataSource(OutputDataSourceProperties properties){
//        return properties.initializeDataSourceBuilder().build();
//    }
//    @Bean
//    public DataSourceTransactionManager inputTransactionManager(DataSource inputDataSource) {
//        DataSourceTransactionManager transactionManager = new DataSourceTransactionManager(inputDataSource);
//        return transactionManager;
//    }
//
//    @Bean
//    public DataSourceTransactionManager outputTransactionManager(DataSource outputDataSource) {
//        DataSourceTransactionManager transactionManager = new DataSourceTransactionManager(outputDataSource);
//        return transactionManager;
//    }
//}
