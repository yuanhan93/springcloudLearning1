package com.just.springcloud.ui.controller;

import com.just.springcloud.ui.domain.Person;
import com.just.springcloud.ui.service.PersonHystrixService;
import com.just.springcloud.ui.service.SomeHystrixService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UIController {
    @Autowired
    private SomeHystrixService someHystrixService;
    @Autowired
    private PersonHystrixService personHystrixService;
    @RequestMapping(value = "/dispatch")
    public List<Person> sendMessage(@RequestBody String personName){
        return personHystrixService.save(personName);
    }
    @RequestMapping(value = "getsome",produces = MediaType.TEXT_PLAIN_VALUE)
    public String getSome(){
        return someHystrixService.getSome();
    }
}
