package com.just.springcloud.streamschemaout1;
import com.just.springcloud.streamschemaout1.model.PersonSource;
import com.just.springcloud.streamschemaout1.schema.MessageStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.schema.client.EnableSchemaRegistryClient;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@EnableKafka
@EnableSchemaRegistryClient
@EnableBinding({MessageStream.class})
@RestController
public class StreamSchemaOut1Application {

    @Autowired
    private MessageStream messageStream;
    public static void main(String[] args) {
        SpringApplication.run(StreamSchemaOut1Application.class, args);
    }
    @GetMapping("/test")
    public String test(@RequestParam int count){
        sendCountMessage(count);
        return "ok";
    }
    public void sendCountMessage(int count){
        for(int i=1;i<=count;i++){
            PersonSource personSource=PersonSource.newBuilder()
                    .setName("小明")
                    .setAge(i)
                    .build();
            this.messageStream.output().send(MessageBuilder.withPayload(personSource).build());
        }
    }
}
