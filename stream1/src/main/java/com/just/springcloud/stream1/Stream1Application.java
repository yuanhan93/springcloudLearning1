package com.just.springcloud.stream1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Processor;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableEurekaClient
@EnableKafka
@EnableBinding(Processor.class)
@RestController
public class Stream1Application {

    Logger logger=LoggerFactory.getLogger(Stream1Application.class);
    @Autowired
    private Processor processor;

    public static void main(String[] args) {
        SpringApplication.run(Stream1Application.class, args);
    }

    @StreamListener(target = Sink.INPUT)
    public void handleSinkMessage(String message){
        logger.info("二狗子默认通道收到消息:"+message);
    }

    @GetMapping(value = "/pub/default")
    public Object pubDefault(@RequestParam String message){
        processor.output().send(MessageBuilder.withPayload(message).build());
        return "消息发送成功";
    }
}
