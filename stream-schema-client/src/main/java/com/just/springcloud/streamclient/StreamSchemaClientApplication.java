package com.just.springcloud.streamclient;

import com.just.springcloud.streamclient.model.PersonSource;
import com.just.springcloud.streamclient.schema.MessageStream;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Processor;
import org.springframework.cloud.stream.schema.client.EnableSchemaRegistryClient;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableEurekaClient
@RestController
@RequestMapping("/api")
@EnableKafka
@EnableSchemaRegistryClient
@EnableBinding({MessageStream.class,Processor.class})
//@EnableAutoConfiguration(exclude = {
//        DataSourceAutoConfiguration.class,
//        DataSourceTransactionManagerAutoConfiguration.class})
public class StreamSchemaClientApplication {

    @Autowired
    private MessageStream stream;
    @Autowired
    private Processor processor;

    public static void main(String[] args) {
        SpringApplication.run(StreamSchemaClientApplication.class, args);
    }

    /**
     * 模拟外部消息发送
     * @param personVO
     * @return
     */
    @ApiOperation(value = "模拟发送person消息")
    @PostMapping(value = "/pub")
    public Object publishMessage(@RequestBody PersonVO personVO){
        PersonSource personSource=PersonSource.newBuilder()
                .setName(personVO.getName())
                .setAge(personVO.getAge())
                .build();
        this.stream.output().send(MessageBuilder.withPayload(personSource).build());
        return "消息发送成功";
    }
    @ApiOperation(value ="模拟向默认通道发消息" )
    @PostMapping(value = "/pub/default")
    public Object pubDefault(@RequestParam String message){
        processor.output().send(MessageBuilder.withPayload(message).build());
        return "消息发送成功";
    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    private static class PersonVO{
        private String name;
        private Integer age;
    }
}
