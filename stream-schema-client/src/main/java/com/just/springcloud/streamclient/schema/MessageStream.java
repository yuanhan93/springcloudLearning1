package com.just.springcloud.streamclient.schema;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;
import org.springframework.stereotype.Component;

@Component
public interface MessageStream {
    @Input("person_input")
    SubscribableChannel input();
    @Output("person_output")
    MessageChannel output();
}
