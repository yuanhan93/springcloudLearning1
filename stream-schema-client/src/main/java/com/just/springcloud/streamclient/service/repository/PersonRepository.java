package com.just.springcloud.streamclient.service.repository;

import com.just.springcloud.streamclient.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends JpaRepository<Person,Integer> {
}
