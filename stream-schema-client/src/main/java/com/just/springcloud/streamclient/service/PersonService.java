package com.just.springcloud.streamclient.service;

import com.just.springcloud.streamclient.model.PersonSource;

public interface PersonService {
    void insert(PersonSource personSource);
}
