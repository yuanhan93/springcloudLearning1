package com.just.springcloud.streamclient.schema;

import com.just.springcloud.streamclient.model.PersonSource;
import com.just.springcloud.streamclient.service.PersonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MessageStreamListener {
    private static final Logger logger=LoggerFactory.getLogger(MessageStreamListener.class);
    @Autowired
    private PersonService personService;

    @StreamListener(target = Sink.INPUT)
    public void handleSinkMessage(String message){
        logger.info("二狗子默认通道收到消息:"+message);
    }

    @StreamListener(target = "person_input")
    public void handlePersonMessage(PersonSource personSource){
        //logger.info("person_input 通道收到消息:"+personSource);
        //logger.info("p:"+personSource);
        personService.insert(personSource);
    }
}
